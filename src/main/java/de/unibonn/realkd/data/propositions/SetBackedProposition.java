/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-15 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.data.propositions;

import de.unibonn.realkd.common.IndexSet;

/**
 * @author Mario Boley
 * 
 * @since 0.2.0
 * 
 * @version 0.2.0
 *
 */
public class SetBackedProposition implements Proposition {

	private int id;
	
	private final IndexSet supportSet;

	private String name;

	public SetBackedProposition(int id, IndexSet supportSet) {
		this.id = id;
		this.name = String.valueOf(id);
		this.supportSet = supportSet;
	}
	
	public SetBackedProposition(int id, String name, IndexSet supportSet) {
		this.id = id;
		this.name = name;
		this.supportSet = supportSet;
	}

	@Override
	public boolean holdsFor(int i) {
		return supportSet.contains(i);
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public IndexSet getSupportSet() {
		return supportSet;
	}

	@Override
	public int getSupportCount() {
		return supportSet.size();
	}
	
	public String toString() {
		return this.name;
	}

	@Override
	public String name() {
		return name;
	}

}
