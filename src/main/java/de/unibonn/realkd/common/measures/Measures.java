/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2017 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.common.measures;

import static de.unibonn.realkd.util.Comparison.equalsIncludingNaN;

import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableList;

/**
 * @author Mario Boley
 * 
 * @since 0.5.0
 * 
 * @version 0.5.0
 *
 */
public class Measures {

	private Measures() {
		;
	}

	public static Measurement measurement(Measure measureId, double value) {
		return new MeasurementImplementation(measureId, value);
	}

	public static Measurement measurement(Measure measureId, double value, List<Measurement> auxiliaryMeasurements) {
		return new MeasurementImplementation(measureId, value, auxiliaryMeasurements);
	}

	private static class MeasurementImplementation implements Measurement {

		private final Measure measure;

		private final double value;

		private final List<Measurement> auxiliaryMeasurements;

		private final String stringRepr;

		private MeasurementImplementation(Measure measure, double value) {
			this(measure, value, ImmutableList.of());
		}

		@JsonCreator
		private MeasurementImplementation(@JsonProperty("measure") Measure measure, @JsonProperty("value") double value,
				@JsonProperty("auxiliaryMeasurements") List<Measurement> auxiliaryMeasurements) {
			this.measure = measure;
			this.value = value;
			this.auxiliaryMeasurements = auxiliaryMeasurements;
			this.stringRepr = measure.caption() + ": " + value;
		}

		@Override
		@JsonProperty("measure")
		public Measure measure() {
			return measure;
		}

		@Override
		@JsonProperty("value")
		public double value() {
			return value;
		}

		@Override
		@JsonProperty("auxiliaryMeasurements")
		public List<Measurement> auxiliaryMeasurements() {
			return auxiliaryMeasurements;
		}

		public String toString() {
			return stringRepr;
		}

		public boolean equals(Object other) {
			if (this == other) {
				return true;
			}
			if (!(this instanceof Measurement)) {
				return false;
			}
			return this.measure.equals(((Measurement) other).measure())
					&& equalsIncludingNaN(this.value, (((Measurement) other).value()));
		}

		public int hashCode() {
			return Objects.hash(measure, value);
		}

	}
}
