/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-15 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.patterns.emm;

import static de.unibonn.realkd.patterns.Frequency.FREQUENCY;
import static de.unibonn.realkd.patterns.subgroups.Subgroups.representativenessMeasurement;
import static java.lang.Math.max;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableList;

import de.unibonn.realkd.common.measures.Measure;
import de.unibonn.realkd.common.measures.Measurement;
import de.unibonn.realkd.common.measures.Measures;
import de.unibonn.realkd.common.workspace.SerialForm;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.propositions.AttributeBasedProposition;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;
import de.unibonn.realkd.data.table.attribute.OrdinalAttribute;
import de.unibonn.realkd.patterns.DefaultPattern;
import de.unibonn.realkd.patterns.MeasurementProcedure;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.PatternBuilder;
import de.unibonn.realkd.patterns.PatternDescriptor;
import de.unibonn.realkd.patterns.logical.LogicalDescriptor;
import de.unibonn.realkd.patterns.models.Model;
import de.unibonn.realkd.patterns.models.ModelDistanceFunction;
import de.unibonn.realkd.patterns.models.ModelFactory;
import de.unibonn.realkd.patterns.models.UnivariateOrdinalProbabilisticModel;
import de.unibonn.realkd.patterns.models.event.SingleEventModel;
import de.unibonn.realkd.patterns.models.mean.MetricEmpiricalDistribution;
import de.unibonn.realkd.patterns.models.regression.LinearRegressionModel;
import de.unibonn.realkd.patterns.subgroups.ControlledSubgroup;
import de.unibonn.realkd.patterns.subgroups.Subgroup;
import de.unibonn.realkd.patterns.subgroups.Subgroups;

/**
 * <p>
 * Provides factory method for exceptional model patterns and measurement
 * procedures applicable to EMM pattern descriptors.
 * </p>
 * 
 * @author Mario Boley
 * @author Sandy Moens
 * 
 * @since 0.1.2
 * 
 * @version 0.5.0
 *
 */
public class ExceptionalModelMining {

	public static ExceptionalModelPattern emmPattern(Subgroup<?> descriptor,
			MeasurementProcedure<? extends ModelDeviationMeasure, ? super PatternDescriptor> distanceMeasurementProcedure,
			List<MeasurementProcedure<? extends Measure, ? super PatternDescriptor>> additionalMeasurementProcedures) {
		List<Measurement> measurements = new ArrayList<>();
		measurements.add(FREQUENCY.perform(descriptor));
		measurements.add(distanceMeasurementProcedure.perform(descriptor));
		measurements.addAll(Subgroups.accuracyGainMeasurements(descriptor));
		// measurements.addAll(Subgroups.descriptiveModelMeasurements(descriptor));

		List<MeasurementProcedure<? extends Measure, ? super PatternDescriptor>> procedures = additionalMeasurementProcedures
				.stream().filter(p -> p.isApplicable(descriptor)).collect(Collectors.toList());

		measurements.addAll(procedures.stream().map(p -> p.perform(descriptor)).collect(Collectors.toList()));
		if (descriptor instanceof ControlledSubgroup<?, ?>) {
			representativenessMeasurement((ControlledSubgroup<?, ?>) descriptor).ifPresent(m -> measurements.add(m));
		}

		return new ExceptionalModelPatternImplementation(descriptor, measurements, distanceMeasurementProcedure,
				ImmutableList.copyOf(procedures));
	}

	public static final ModelDistanceMeasurementProcedure MANHATTEN_MEAN_DEV_MEASUREMENT_PROCEDURE = new ModelDistanceFunctionBasedMeasurementProcedure(
			MetricEmpiricalDistribution.MANHATTEN_MEAN_DEVIATION);

	public static final MeasurementProcedure<ModelDeviationMeasure, PatternDescriptor> ABS_NORMALIZED_MEDIAN_SHIFT_PROCEDURE = UnivariateMeanModelDeviationMeasurementProcedures.ABSOLUTE_NORMALIZED_MEDIAN_SHIFT_PROCEDURE;

	public static final MeasurementProcedure<ModelDeviationMeasure, PatternDescriptor> POS_NORMALIZED_MEDIAN_SHIFT_PROCEDURE = UnivariateMeanModelDeviationMeasurementProcedures.POSITIVE_NORMALIZED_MEDIAN_SHIFT_PROCEDURE;

	public static final MeasurementProcedure<ModelDeviationMeasure, PatternDescriptor> NEG_NORMALIZED_MEDIAN_SHIFT_PROCEDURE = UnivariateMeanModelDeviationMeasurementProcedures.NEGATIVE_NORMALIZED_MEDIAN_SHIFT_PROCEDURE;
	//
	// new ModelDistanceFunctionBasedMeasurementProcedure(
	// MetricEmpiricalDistribution.MEDIAN_DEVIATION);

	public static final MeasurementProcedure<ModelDeviationMeasure, PatternDescriptor> POS_PROBABILITY_SHIFT_PROCEDURE = SingleEventModelDeviationMeasurementProcedures.INSTANCE;

	public static final MeasurementProcedure<ModelDeviationMeasure, PatternDescriptor> POS_NORMALIZED_MEAN_SHIFT_PROCEDURE = UnivariateMeanModelDeviationMeasurementProcedures.POSITIVE_NORMALIZED_MEAN_SHIFT_PROCEDURE;

	public static final MeasurementProcedure<ModelDeviationMeasure, PatternDescriptor> NEG_NORMALIZED_MEAN_SHIFT_PROCEDURE = UnivariateMeanModelDeviationMeasurementProcedures.NEGATIVE_NORMALIZED_MEAN_SHIFT_PROCEDURE;

	public static final MeasurementProcedure<ModelDeviationMeasure, PatternDescriptor> ABS_NORMALIZED_MEAN_SHIFT_PROCEDURE = UnivariateMeanModelDeviationMeasurementProcedures.ABSOLUTE_NORMALIZED_MEAN_SHIFT_PROCEDURE;

	public static final ModelDistanceMeasurementProcedure COS_DIST_PROCEDURE = new ModelDistanceFunctionBasedMeasurementProcedure(
			LinearRegressionModel.COSINEDISTANCE);

	public static final MeasurementProcedure<ModelDeviationMeasure, PatternDescriptor> EMP_KOLMOGOROV_SMIRNOV = UnivariateOrdinalModelDeviationMeasurementProcedures.EMPIRICAL_KOLMOGOROV_SMIRNOV_PROCEDURE;

	/**
	 * Complete collection of distance based measurement procedures defined in
	 * this class.
	 */
	public static final List<MeasurementProcedure<ModelDeviationMeasure, PatternDescriptor>> MODEL_DISTANCE_BASED_MEASUREMENT_PROCEDURES = ImmutableList
			.of(TotalVariationDistance.TOTAL_VARIATION_DISTANCE, HellingerDistance.HELLINGER_DISTANCE,
					POS_PROBABILITY_SHIFT_PROCEDURE, MANHATTEN_MEAN_DEV_MEASUREMENT_PROCEDURE,
					POS_NORMALIZED_MEAN_SHIFT_PROCEDURE, NEG_NORMALIZED_MEAN_SHIFT_PROCEDURE,
					ABS_NORMALIZED_MEAN_SHIFT_PROCEDURE, COS_DIST_PROCEDURE, CumulativeJensenShannonDivergence.CJS, EMP_KOLMOGOROV_SMIRNOV);

	public MeasurementProcedure<ModelDeviationMeasure, PatternDescriptor> measurementProcedure(
			ModelDeviationMeasure measure) {
		return measurementProcedure(measure);
	}

	private static final Map<Measure, MeasurementProcedure<ModelDeviationMeasure, PatternDescriptor>> measuresToDefaultProcedures = new HashMap<>();
	static {
		MODEL_DISTANCE_BASED_MEASUREMENT_PROCEDURES.forEach(p -> measuresToDefaultProcedures.put(p.getMeasure(), p));
	}

	private static enum SingleEventModelDeviationMeasurementProcedures
			implements MeasurementProcedure<ModelDeviationMeasure, PatternDescriptor> {

		INSTANCE;

		@Override
		public boolean isApplicable(PatternDescriptor descriptor) {
			return (descriptor instanceof Subgroup
					&& ((Subgroup<?>) descriptor).localModel() instanceof SingleEventModel);
		}

		@Override
		public ModelDeviationMeasure getMeasure() {
			return ConcreteModelDeviationMeasure.POSITIVE_PROBABILITY_SHIFT;
		}

		@Override
		public Measurement perform(PatternDescriptor descriptor) {
			Subgroup<?> subgroup = (Subgroup<?>) descriptor;
			SingleEventModel localModel = (SingleEventModel) subgroup.localModel();
			SingleEventModel referenceModel = (SingleEventModel) subgroup.referenceModel();
			return Measures.measurement(getMeasure(), max(localModel.probability() - referenceModel.probability(), 0));
		}

		@Override
		public String toString() {
			return getMeasure().caption();
		}

	}

	private static enum UnivariateOrdinalModelDeviationMeasurementProcedures
			implements MeasurementProcedure<ModelDeviationMeasure, PatternDescriptor> {

		/**
		 * Measures the Kolmogorov-Smirnov statistic for an ordinal attribute by
		 * iterating over the empirical data points in ascending order of their
		 * values.
		 */
		EMPIRICAL_KOLMOGOROV_SMIRNOV_PROCEDURE;

		@Override
		public boolean isApplicable(PatternDescriptor descriptor) {
			return (descriptor instanceof Subgroup)
					&& (((Subgroup<?>) descriptor).localModel() instanceof UnivariateOrdinalProbabilisticModel);
		}

		@Override
		public ModelDeviationMeasure getMeasure() {
			return ConcreteModelDeviationMeasure.KOLMOGOROV_SMIRNOV_STATISTIC;
		}

		@Override
		public Measurement perform(PatternDescriptor descriptor) {
			Subgroup<?> subgroup = (Subgroup<?>) descriptor;
			OrdinalAttribute<?> target = (OrdinalAttribute<?>) subgroup.targetAttributes().get(0);

			double maxDiff = 0.0;
			int subCumCount = 0;
			int globalNonMissingCount = target.sortedNonMissingRowIndices().size();
			int localNonMissingCount = (int) subgroup.supportSet().stream().filter(i -> !target.valueMissing(i)).count();
			if (localNonMissingCount == 0) {
				return Measures.measurement(getMeasure(), Double.NaN);
			}
			for (int i = 0; i < globalNonMissingCount; i++) {
				int obj = target.sortedNonMissingRowIndices().get(i);
				if (subgroup.supportSet().contains(obj)) {
					subCumCount++;
				}
				double diff = Math
						.abs((double) i / globalNonMissingCount - (double) subCumCount / localNonMissingCount);
				maxDiff = Math.max(maxDiff, diff);
			}
			return Measures.measurement(getMeasure(), maxDiff);
		}

		public String toString() {
			return getMeasure().caption();
		}

	}

	private static enum UnivariateMeanModelDeviationMeasurementProcedures
			implements MeasurementProcedure<ModelDeviationMeasure, PatternDescriptor> {

		POSITIVE_NORMALIZED_MEAN_SHIFT_PROCEDURE {

			@Override
			public ModelDeviationMeasure getMeasure() {
				return ConcreteModelDeviationMeasure.POSITIVE_NORMALIZED_MEAN_SHIFT;
			}

			@Override
			protected double value(MetricEmpiricalDistribution refModel, MetricEmpiricalDistribution localModel,
					MetricAttribute attribute) {
				double distance = Math.max(0, localModel.means().get(0) - refModel.means().get(0));
				return distance / (attribute.max() - attribute.mean());
			}

		},
		NEGATIVE_NORMALIZED_MEAN_SHIFT_PROCEDURE {
			@Override
			public ModelDeviationMeasure getMeasure() {
				return ConcreteModelDeviationMeasure.NEGATIVE_NORMALIZED_MEAN_SHIFT;
			}

			@Override
			protected double value(MetricEmpiricalDistribution refModel, MetricEmpiricalDistribution localModel,
					MetricAttribute attribute) {
				double distance = Math.max(0, refModel.means().get(0) - localModel.means().get(0));
				return distance / (attribute.mean() - attribute.min());
			}
		},
		ABSOLUTE_NORMALIZED_MEAN_SHIFT_PROCEDURE {
			@Override
			public ModelDeviationMeasure getMeasure() {
				return ConcreteModelDeviationMeasure.ABSOLUTE_NORMALIZED_MEAN_SHIFT;
			}

			@Override
			protected double value(MetricEmpiricalDistribution refModel, MetricEmpiricalDistribution localModel,
					MetricAttribute attribute) {
				double distance = Math.abs(refModel.means().get(0) - localModel.means().get(0));
				return distance / Math.max(attribute.max() - attribute.mean(), attribute.mean() - attribute.min());
			}
		},
		POSITIVE_NORMALIZED_MEDIAN_SHIFT_PROCEDURE {

			@Override
			public ModelDeviationMeasure getMeasure() {
				return ConcreteModelDeviationMeasure.POSITIVE_NORMALIZED_MEDIAN_SHIFT;
			}

			@Override
			protected double value(MetricEmpiricalDistribution refModel, MetricEmpiricalDistribution localModel,
					MetricAttribute attribute) {
				double distance = Math.max(0, localModel.medians().get(0) - refModel.medians().get(0));
				return distance / (attribute.max() - attribute.median());
			}

		},
		NEGATIVE_NORMALIZED_MEDIAN_SHIFT_PROCEDURE {
			@Override
			public ModelDeviationMeasure getMeasure() {
				return ConcreteModelDeviationMeasure.NEGATIVE_NORMALIZED_MEDIAN_SHIFT;
			}

			@Override
			protected double value(MetricEmpiricalDistribution refModel, MetricEmpiricalDistribution localModel,
					MetricAttribute attribute) {
				double distance = Math.max(0, refModel.medians().get(0) - localModel.medians().get(0));
				return distance / (attribute.median() - attribute.min());
			}
		},
		ABSOLUTE_NORMALIZED_MEDIAN_SHIFT_PROCEDURE {

			@Override
			public ModelDeviationMeasure getMeasure() {
				return ConcreteModelDeviationMeasure.ABSOLUTE_NORMALIZED_MEDIAN_SHIFT;
			}

			@Override
			protected double value(MetricEmpiricalDistribution refModel, MetricEmpiricalDistribution localModel,
					MetricAttribute attribute) {
				double distance = Math.abs(localModel.medians().get(0) - refModel.medians().get(0));
				return distance / Math.max(attribute.max() - attribute.median(), attribute.median() - attribute.min());
			}

		};

		@Override
		public boolean isApplicable(PatternDescriptor descriptor) {
			if (!(descriptor instanceof Subgroup)) {
				return false;
			}
			Subgroup<?> subgroup = (Subgroup<?>) descriptor;
			Model referenceModel = subgroup.referenceModel();
			Model localModel = subgroup.localModel();
			return (referenceModel instanceof MetricEmpiricalDistribution
					&& localModel instanceof MetricEmpiricalDistribution
					&& ((MetricEmpiricalDistribution) referenceModel).means().size() == 1
					&& ((MetricEmpiricalDistribution) localModel).means().size() == 1);
		}

		@Override
		public abstract ModelDeviationMeasure getMeasure();

		protected abstract double value(MetricEmpiricalDistribution refModel, MetricEmpiricalDistribution localModel,
				MetricAttribute attribute);

		@Override
		public Measurement perform(PatternDescriptor descriptor) {
			Subgroup<?> subgroup = (Subgroup<?>) descriptor;
			MetricAttribute attribute = (MetricAttribute) subgroup.targetAttributes().get(0);

			double value = value((MetricEmpiricalDistribution) subgroup.referenceModel(),
					(MetricEmpiricalDistribution) subgroup.localModel(), attribute);

			return Measures.measurement(getMeasure(), value);
		}

		public String toString() {
			return getMeasure().caption();
		}

	}

	private static class ModelDistanceFunctionBasedMeasurementProcedure implements ModelDistanceMeasurementProcedure {

		private final ModelDistanceFunction distanceFunction;

		@JsonCreator
		public ModelDistanceFunctionBasedMeasurementProcedure(
				@JsonProperty("distanceFunction") ModelDistanceFunction distanceFunction) {
			this.distanceFunction = distanceFunction;
		}

		@SuppressWarnings("unused") // for Jackson
		public ModelDistanceFunction getDistanceFunction() {
			return distanceFunction;
		}

		@Override
		@JsonIgnore
		public final ModelDeviationMeasure getMeasure() {
			return distanceFunction.getCorrespondingInterestingnessMeasure();
		}

		@Override
		public String toString() {
			return distanceFunction.toString();
		}

		public boolean equals(Object other) {
			if (this == other) {
				return true;
			}
			if (!(other instanceof ModelDistanceFunctionBasedMeasurementProcedure)) {
				return false;
			}
			return (distanceFunction.equals(((ModelDistanceFunctionBasedMeasurementProcedure) other).distanceFunction));
		}

		@Override
		public boolean isApplicable(Model globalModel, Model localModel) {
			return distanceFunction.isApplicable(globalModel, localModel);
		}

		@Override
		public Measurement perform(Model g, Model l) {
			return Measures.measurement(getMeasure(), distanceFunction.distance(g, l));
		}

	}

	private static class ExceptionalModelPatternImplementation extends DefaultPattern<Subgroup<?>>
			implements ExceptionalModelPattern {

		private final Subgroup<?> descriptor;

		private final MeasurementProcedure<? extends ModelDeviationMeasure, ? super PatternDescriptor> deviationMeasurementProcedure;

		private final List<MeasurementProcedure<? extends Measure, ? super PatternDescriptor>> additionalMeasurementProcedureBackup;

		private ExceptionalModelPatternImplementation(Subgroup<?> descriptor, List<Measurement> measurements,
				MeasurementProcedure<? extends ModelDeviationMeasure, ? super PatternDescriptor> deviationMeasurementProcedure,
				List<MeasurementProcedure<? extends Measure, ? super PatternDescriptor>> additionalMeasurementProcedures) {

			super(descriptor.extensionDescriptor().getPropositionalLogic().population(), descriptor, measurements);

			this.descriptor = descriptor;
			this.deviationMeasurementProcedure = deviationMeasurementProcedure;
			this.additionalMeasurementProcedureBackup = additionalMeasurementProcedures;
		}

		@Override
		public Subgroup<?> descriptor() {
			return this.descriptor;
		}

		@Override
		public ModelDeviationMeasure getDeviationMeasure() {
			return deviationMeasurementProcedure.getMeasure();
		}

		@Override
		public SerialForm<ExceptionalModelPattern> serialForm() {
			return new ExceptionalModelPatternBuilderImplementation(descriptor.serialForm(),
					this.deviationMeasurementProcedure, this.additionalMeasurementProcedureBackup);
		}
		
		@Override
		protected String typeString() {
			return "ExceptionalPattern";
		}
		
	}

	private static class ExceptionalModelPatternBuilderImplementation
			implements PatternBuilder<Subgroup<?>, ExceptionalModelPattern> {

		private final SerialForm<? extends Subgroup<?>> descriptorBuilder;

		private final ArrayList<MeasurementProcedure<? extends Measure, ? super PatternDescriptor>> additionalMeasurementProcedures;

		private final MeasurementProcedure<? extends ModelDeviationMeasure, ? super PatternDescriptor> distanceMeasurementProcedure;

		@JsonCreator
		private ExceptionalModelPatternBuilderImplementation(
				@JsonProperty("descriptorBuilder") SerialForm<? extends Subgroup<?>> descriptorBuilder,
				@JsonProperty("distanceMeasurementProcedure") MeasurementProcedure<? extends ModelDeviationMeasure, ? super PatternDescriptor> distanceMeasurementProcedure,
				@JsonProperty("additionalMeasurementProcedures") List<MeasurementProcedure<? extends Measure, ? super PatternDescriptor>> additionalMeasurementProcedures) {
			this.descriptorBuilder = descriptorBuilder;
			this.distanceMeasurementProcedure = distanceMeasurementProcedure;
			this.additionalMeasurementProcedures = new ArrayList<>(additionalMeasurementProcedures);
		}

		@SuppressWarnings("unused") // for Jackson
		public MeasurementProcedure<? extends Measure, ? super PatternDescriptor> getDistanceMeasurementProcedure() {
			return distanceMeasurementProcedure;
		}

		// @SuppressWarnings("unused") // for Jackson
		// public void
		// distanceMeasureProcedure(ModelDistanceMeasurementProcedure
		// distanceMeasurementProcedure) {
		// this.distanceMeasurementProcedure = distanceMeasurementProcedure;
		// }

		@SuppressWarnings("unused") // for Jackson
		public List<MeasurementProcedure<? extends Measure, ? super PatternDescriptor>> getAdditionalMeasurementProcedures() {
			return additionalMeasurementProcedures;
		}

		@Override
		public synchronized ExceptionalModelPattern build(Workspace workspace) {
			return emmPattern(descriptorBuilder.build(workspace), distanceMeasurementProcedure,
					additionalMeasurementProcedures);
		}

		@Override
		public synchronized SerialForm<? extends Subgroup<?>> getDescriptorBuilder() {
			return descriptorBuilder;
		}

		@Override
		public boolean equals(Object other) {
			if (this == other) {
				return true;
			}
			if (!(other instanceof ExceptionalModelPatternBuilderImplementation)) {
				return false;
			}
			ExceptionalModelPatternBuilderImplementation otherBuilder = (ExceptionalModelPatternBuilderImplementation) other;
			return (this.descriptorBuilder.equals(otherBuilder.descriptorBuilder)
					&& this.distanceMeasurementProcedure.equals(otherBuilder.distanceMeasurementProcedure)
					&& this.additionalMeasurementProcedures.equals(otherBuilder.additionalMeasurementProcedures));
		}

		// @Override
		// public List<MeasurementProcedure> getMeasurementProcedures() {
		// ArrayList<MeasurementProcedure> result = new ArrayList<>();
		// result.add(FREQUENCY_FOR_EMMPATTERNS_PROCEDURE);
		// result.add(distanceMeasurementProcedure);
		// result.addAll(additionalMeasurementProcedures);
		// return result;
		// }

	}

	/**
	 * Creates a function mapping extension descriptors to exceptional subgroup
	 * patterns; using a fixed sets of target attributes, a reference model, and
	 * a model factory for fitting the local model.
	 * 
	 * @param table
	 *            the data table containing the target attributes
	 * @param targetAttr
	 *            target attributes
	 * @param modelFactory
	 *            factory for fitting the local model
	 * @param referenceModel
	 *            a constant reference model
	 * @param distanceMeasurementProc
	 *            the procedure for measuring model distance
	 * @param additionalProcedures
	 *            other measurement procedures
	 * @return map from extension descriptor to exceptional subgroup
	 */
	public static Function<LogicalDescriptor, ExceptionalModelPattern> extensionDescriptorToEmmPatternMap(
			final DataTable table, final List<Attribute<?>> targetAttr, final ModelFactory<?> modelFactory,
			final Model referenceModel,
			final MeasurementProcedure<ModelDeviationMeasure, ? super PatternDescriptor> distanceMeasurementProc,
			List<MeasurementProcedure<? extends Measure, ? super PatternDescriptor>> additionalProcedures) {
		return d -> {
			Model localModel = modelFactory.getModel(table, targetAttr, d.supportSet());
			Subgroup<?> subgroup = Subgroups.subgroup(d, table, targetAttr, modelFactory, referenceModel, localModel);
			return emmPattern(subgroup, distanceMeasurementProc, additionalProcedures);
		};
	}

	/**
	 * Creates a function mapping extension descriptors to exceptional subgroup
	 * patterns; using a fixed sets of target attributes, a reference model, and
	 * a model factory for fitting the local model.
	 * 
	 * @param table
	 *            the data table containing the target attributes
	 * @param targetAttr
	 *            target attributes
	 * @param modelFactory
	 *            factory for fitting the local model
	 * @param referenceModel
	 *            a constant reference model
	 * @param distanceMeasurementProc
	 *            the procedure for measuring model distance
	 * @param additionalProcedures
	 *            other measurement procedures
	 * @return map from extension descriptor to exceptional subgroup
	 */
	public static <T extends Model, C extends Model> Function<LogicalDescriptor, ExceptionalModelPattern> extensionDescriptorToControlledEmmPatternMap(
			final DataTable table, final List<Attribute<?>> targetAttr, final ModelFactory<? extends T> modelFactory,
			final T referenceModel,
			final MeasurementProcedure<ModelDeviationMeasure, ? super PatternDescriptor> distanceMeasurementProc,
			final List<MeasurementProcedure<? extends Measure, ? super PatternDescriptor>> additionalProcedures,
			final List<Attribute<?>> controlAttributes, final C referenceControlModel,
			final ModelFactory<C> controlModelFactory) {
		return d -> {
			T localModel = modelFactory.getModel(table, targetAttr, d.supportSet());
			C localControlModel = controlModelFactory.getModel(table, controlAttributes, d.supportSet());
			ControlledSubgroup<T, C> subgroup = Subgroups.controlledSubgroup(d, table, targetAttr, modelFactory,
					referenceModel, localModel, controlAttributes, controlModelFactory, referenceControlModel,
					localControlModel);
			return emmPattern(subgroup, distanceMeasurementProc, additionalProcedures);
		};
	}
	
	public static <T extends Model, C extends Model> Function<LogicalDescriptor, ExceptionalModelPattern> extensionDescriptorToControlledEmmPatternMap(
			final DataTable table, final List<Attribute<? extends Object>> targetAttr, final ModelFactory<? extends T> modelFactory,
			final MeasurementProcedure<ModelDeviationMeasure, ? super PatternDescriptor> distanceMeasurementProc,
			final List<Attribute<? extends Object>> controlAttributes,
			final ModelFactory<C> controlModelFactory) {
		T referenceModel= modelFactory.getModel(table, targetAttr);
		C referenceControlModel = controlModelFactory.getModel(table, controlAttributes);
		return d -> {
			T localModel = modelFactory.getModel(table, targetAttr, d.supportSet());
			C localControlModel = controlModelFactory.getModel(table, controlAttributes,d.supportSet());
			ControlledSubgroup<T, C> subgroup = Subgroups.controlledSubgroup(d, table, targetAttr, modelFactory,
					referenceModel, localModel, controlAttributes, controlModelFactory, referenceControlModel,
					localControlModel);
			return emmPattern(subgroup, distanceMeasurementProc, ImmutableList.of());
		};
	}

	private static final Function<Pattern<?>, LogicalDescriptor> EXCEPTIONAL_SUBGROUP_TO_EXT_DESCR_MAP = pattern -> ((Subgroup<?>) pattern
			.descriptor()).extensionDescriptor();

	public static Function<Pattern<?>, LogicalDescriptor> exceptionalSubgroupToExtensionDescriptor() {
		return EXCEPTIONAL_SUBGROUP_TO_EXT_DESCR_MAP;
	}

	/**
	 * @return a predicate for propositions that is false if and only if
	 *         proposition relates to at least one of a set of target attributes
	 */
	public static Predicate<Proposition> targetAttributeFilter(DataTable dataTable, List<Attribute<?>> targets) {
		return p -> {
			if (!(p instanceof AttributeBasedProposition)) {
				return true;
			}

			Attribute<?> attribute = ((AttributeBasedProposition<?>) p).getAttribute();
			if (targets.contains(attribute) || dataTable.containsDependencyBetweenAnyOf(attribute, targets)) {
				return false;
			}
			return true;
		};
	}

}
