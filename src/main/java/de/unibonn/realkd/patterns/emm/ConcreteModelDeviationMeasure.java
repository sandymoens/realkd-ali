/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-16 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.patterns.emm;

/**
 * @author Mario Boley
 * 
 * @since 0.4.0
 * 
 * @version 0.4.0
 *
 */
public enum ConcreteModelDeviationMeasure implements ModelDeviationMeasure {
	
	POSITIVE_PROBABILITY_SHIFT("Positive probability shift","Difference between event probability in subgroup and refernce event probability or zero if difference negative."),
	MANHATTAN_MEAN_DISTANCE("Manhattan mean distance", "Manhatten distance between mean vector of global population and mean vector of subgroup."),
	EUCLIDEAN_MEAN_DISTANCE("Eucledian mean distance", ""),
	POSITIVE_NORMALIZED_MEAN_SHIFT("norm. pos. mean shift", "Difference between mean target attribute value in subgroup and mean target attribute value in global population (divided by global max value minus global mean value) or zero of difference is negative."), 
	NEGATIVE_NORMALIZED_MEAN_SHIFT("norm. neg. mean shift", "Difference between mean target attribute value in global population and mean target attribute value in subgroup (or zero of difference is negative) normalised by difference of max attribute value and global mean value."),
	ABSOLUTE_NORMALIZED_MEAN_SHIFT("norm. abs. mean shift", "Absolute difference between mean target attribute value in global population and mean target attribute value in subgroup (normalized by either the difference of max value and global mean value or the difference of global mean value and min value; whatever difference is larger)"),
	POSITIVE_NORMALIZED_MEDIAN_SHIFT("norm. pos. median shift", "Difference between median target attribute value in subgroup and median target attribute value in global population (divided by global max value minus global median value) or zero of difference is negative."), 
	NEGATIVE_NORMALIZED_MEDIAN_SHIFT("norm. neg. median shift", "Difference between median target attribute value in global population and median target attribute value in subgroup (divided by global median value - minus global min value) or zero of difference is negative."),
	ABSOLUTE_NORMALIZED_MEDIAN_SHIFT("norm. abs. median shift","Absolute difference between median target attribute value in subgroup and median target attribute value in global population."),
	ANGULAR_DISTANCE_OF_SLOPES("cosine distance", "Cosine of the angle between global and local regression models."),
	KOLMOGOROV_SMIRNOV_STATISTIC("Kolmogorov-Smirnov statistic", "Maximum difference between cumulative target distributions of global and subgroup data.");

	private final String name;
	
	private final String description;

	private ConcreteModelDeviationMeasure(String name, String description) {
		this.name = name;
		this.description = description;
	}

	@Override
	public String caption() {
		return name;
	}

	@Override
	public String description() {
		return description;
	}
	
}
