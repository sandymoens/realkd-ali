/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-16 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.algorithms.functional;

import static de.unibonn.realkd.patterns.functional.CoDomainAmbiguityCount.CODOMAIN_AMBIGUITY_COUNT;
import static de.unibonn.realkd.patterns.functional.ExpectedMutualInformation.EXPECTED_MUTUAL_INFORMATION;
import static de.unibonn.realkd.patterns.functional.FunctionalPatterns.correlationDescriptor;
import static de.unibonn.realkd.patterns.functional.FunctionalPatterns.functionalPattern;
import static de.unibonn.realkd.patterns.functional.ReliableFractionOfInformation.RELIABLE_FRACTION_OF_INFORMATION;
import static de.unibonn.realkd.util.Arrays.filteredRange;
import static de.unibonn.realkd.util.Predicates.notSatisfied;
import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toSet;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.ToDoubleFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSet.Builder;

import de.unibonn.realkd.algorithms.AbstractMiningAlgorithm;
import de.unibonn.realkd.algorithms.AlgorithmCategory;
import de.unibonn.realkd.algorithms.ComputationMeasure;
import de.unibonn.realkd.algorithms.branchbound.OPUS;
import de.unibonn.realkd.algorithms.branchbound.OPUS.OperatorOrder;
import de.unibonn.realkd.algorithms.common.MiningParameters;
import de.unibonn.realkd.common.measures.Measure;
import de.unibonn.realkd.common.measures.Measurement;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.Parameters;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.CategoricAttribute;
import de.unibonn.realkd.data.table.attribute.OrdinalAttribute;
import de.unibonn.realkd.patterns.MeasurementProcedure;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.functional.CorrelationDescriptor;
import de.unibonn.realkd.patterns.functional.FunctionalPattern;
import de.unibonn.realkd.patterns.models.table.ContingencyTable;
import de.unibonn.realkd.patterns.models.table.ContingencyTables;

/**
 * @author Panagiotis Mandros
 * 
 * @since 0.5.0
 * 
 * @version 0.5.0
 *
 */
public class OPUSFunctionalPatternSearch extends AbstractMiningAlgorithm implements FunctionalPatternSearch {

	private OPUS<FunctionalPattern, SearchNode> opus;

	private List<Attribute<?>> listOfAttributes;

	private final Parameter<DataTable> datatableParameter;

	private final Parameter<Attribute<?>> targetAttributeParameter;

	private final Parameter<Set<Attribute<?>>> attributeFilter;

	private final Parameter<Integer> numberOfResults;

	private final Parameter<Double> alpha;

	private final Parameter<OperatorOrder> operatorOrder;

	private final Parameter<LanguageOption> languageOption;

	private final Parameter<OptimisticEstimatorOption> optimisticOption;

	public OPUSFunctionalPatternSearch(Workspace workspace) {
		datatableParameter = MiningParameters.dataTableParameter(workspace);

		targetAttributeParameter = Parameters.rangeEnumerableParameter("Target attribute",
				"The target to find dependencies on", Attribute.class,
				() -> datatableParameter.current().attributes().stream()
						.filter(a -> a instanceof CategoricAttribute || a instanceof OrdinalAttribute<?>)
						.collect(Collectors.toList()),
				datatableParameter);

		attributeFilter = Parameters.subSetParameter("Attribute filter", "Attributes that are ignored during search.",
				() -> datatableParameter.current().attributes().stream()
						.filter(a -> a != targetAttributeParameter.current() && !datatableParameter.current()
								.containsDependencyBetween(a, targetAttributeParameter.current()))
						.collect(toSet()),
				targetAttributeParameter);

		numberOfResults = Parameters.integerParameter("Number of results",
				"Number of results, i.e., the size of the result queue.", 1, n -> n > 0, "Specify positive integer.");

		alpha = Parameters.doubleParameter("alpha", "alpha-approximation of the best possible solution", 1,
				n -> n > 0 && n <= 1, "Specify number greater than 0, and smaller or equal to 1");

		operatorOrder = Parameters.rangeEnumerableParameter("Operator assignment",
				"The way in which refinement operators are assigned to search nodes.", OPUS.OperatorOrder.class,
				() -> asList(OperatorOrder.values()));

		languageOption = Parameters.rangeEnumerableParameter("Language",
				"The language of function domains to be searched by algorithm", LanguageOption.class,
				() -> asList(LanguageOption.values()));

		optimisticOption = Parameters.rangeEnumerableParameter("Optimistic estimator",
				"Which optimistic estimator to use", OptimisticEstimatorOption.class,
				() -> asList(OptimisticEstimatorOption.values()));

		registerParameter(datatableParameter);
		registerParameter(targetAttributeParameter);
		registerParameter(attributeFilter);
		registerParameter(numberOfResults);
		registerParameter(alpha);
		registerParameter(operatorOrder);
		registerParameter(languageOption);
		registerParameter(optimisticOption);
	}

	@Override
	public String caption() {
		return "Exhaustive Functional Pattern Discovery";
	}

	@Override
	public String description() {
		return "";
	}

	@Override
	protected void onStopRequest() {
		if (opus != null) {
			opus.requestStop();
		}
	}

	@Override
	public AlgorithmCategory getCategory() {
		return AlgorithmCategory.FUNCTIONAL_PATTERN_DISCOVERY;
	}

	@Override
	public void target(Attribute<?> value) {
		targetAttributeParameter.set(value);
	}

	@Override
	public Attribute<?> target() {
		return targetAttributeParameter.current();
	}

	public DataTable dataTable() {
		return datatableParameter.current();
	}

	@Override
	public void topK(int k) {
		numberOfResults.set(k);
	}

	@Override
	public Integer topK() {
		return numberOfResults.current();
	}

	@Override
	public void alpha(double alpha) {
		this.alpha.set(alpha);
	}

	@Override
	public Double alpha() {
		return alpha.current();
	}

	public void operatorOrder(OperatorOrder order) {
		this.operatorOrder.set(order);
	}

	public OperatorOrder operatorOrder() {
		return operatorOrder.current();
	}

	public void languageOption(LanguageOption langOption) {
		this.languageOption.set(langOption);
	}

	public LanguageOption languageOption() {
		return languageOption.current();
	}

	public void optimisticOption(OptimisticEstimatorOption optimisticOption) {
		this.optimisticOption.set(optimisticOption);
	}

	public OptimisticEstimatorOption optimisticOption() {
		return optimisticOption.current();
	}

	@Override
	protected Collection<Pattern<?>> concreteCall() {

		// get the parameters
		DataTable dataTable = datatableParameter.current();
		Attribute<?> target = targetAttributeParameter.current();

		listOfAttributes = dataTable.attributes().stream()
				.filter(a -> (a instanceof CategoricAttribute || a instanceof OrdinalAttribute<?>) && !(a == target)
						&& !dataTable.containsDependencyBetween(a, target) && !attributeFilter.current().contains(a))
				.collect(Collectors.toList());

		Set<Function<? super SearchNode, ? extends SearchNode>> ops = IntStream.range(0, listOfAttributes.size())
				.mapToObj(i -> (Function<SearchNode, SearchNode>) (n -> n.refine(i))).collect(Collectors.toSet());

		FunctionalPattern rootPattern = functionalPattern(
				correlationDescriptor(dataTable, ImmutableSet.of(), ImmutableSet.of(target())));
		ContingencyTable marginalY = rootPattern.descriptor().contingencyTable().marginal(0);
		double entropyY = marginalY.entropy();
		opus = new OPUS<FunctionalPattern, SearchNode>(n -> n.pattern, ops,
				new SearchNode(rootPattern, marginalY, entropyY), f, optimisticOption().fOEst(), topK(), alpha(),
				Optional.empty(), operatorOrder(), languageOption.current().additionalPruningCriterion());

		Collection<Pattern<?>> result = opus.call();
		return result;
	}

	private class SearchNode {

		public final FunctionalPattern pattern;
		public final ContingencyTable marginalY;
		public final double entropyY;

		public SearchNode(FunctionalPattern pattern, ContingencyTable marginalY, double entropyY) {
			this.pattern = pattern;
			this.marginalY = marginalY;
			this.entropyY = entropyY;
		}

		public String toString() {
			return pattern.toString();
		}

		public SearchNode refine(int i) {
			Set<Attribute<?>> domainAttributes = pattern.descriptor().domain();
			FunctionalPattern newPattern;
			CorrelationDescriptor newRelation;
			SearchNode newSearchNode;
			Builder<Attribute<?>> toBuild = ImmutableSet.<Attribute<?>>builder().addAll(domainAttributes)
					.add(listOfAttributes.get(i));
			ImmutableSet<Attribute<?>> newDomain = toBuild.build();
			newRelation = correlationDescriptor(dataTable(), newDomain, ImmutableSet.of(target()));
			Measurement[] additionalMeasurements = languageOption.current().additionalMeasurementProcedures().stream()
					.map(p -> p.perform(newRelation)).toArray(n -> new Measurement[n]);

			newPattern = functionalPattern(newRelation,
					RELIABLE_FRACTION_OF_INFORMATION.perform(newRelation, entropyY, marginalY), additionalMeasurements);
			newSearchNode = new SearchNode(newPattern, marginalY, entropyY);
			return newSearchNode;
		}
	}

	public static enum LanguageOption {

		ALL {
			@Override
			Predicate<SearchNode> additionalPruningCriterion() {
				return notSatisfied();
			}

			@Override
			ImmutableCollection<MeasurementProcedure<? extends Measure, ? super CorrelationDescriptor>> additionalMeasurementProcedures() {
				return ImmutableList.of();
			}
		},

		MINIMAL_DISAMBIGUATION {
			private boolean canBePruned(SearchNode node) {
				FunctionalPattern pattern = node.pattern;
				CorrelationDescriptor descriptor = pattern.descriptor();
				ContingencyTable table = descriptor.contingencyTable();
				for (int i = 0; i < descriptor.domain().size(); i++) {
					final int l = i;
					int[] projectionIndices = filteredRange(0,
							descriptor.domain().size() + descriptor.coDomain().size(), j -> j != l);
					ContingencyTable projection = table.marginal(projectionIndices);
					int[] disambiguationIndices = IntStream.range(0, descriptor.domain().size() - 1).toArray();
					// here I have to substract the target index
					int projectionAmbiguityCount = projection.ambiguityCount(disambiguationIndices);
					if (pattern.value(CODOMAIN_AMBIGUITY_COUNT) % 1 == 0) {
						if (projectionAmbiguityCount == pattern.value(CODOMAIN_AMBIGUITY_COUNT)) {
							return true;
						}

					} else {
						LOGGER.warning("CODOMAIN_AMBIGUITY_COUNT was not an integer");
					}

				}
				return false;
			}

			private final ImmutableCollection<MeasurementProcedure<? extends Measure, ? super CorrelationDescriptor>> measurementProcedures = ImmutableList
					.of(CODOMAIN_AMBIGUITY_COUNT);

			@Override
			Predicate<SearchNode> additionalPruningCriterion() {
				return this::canBePruned;
			}

			@Override
			ImmutableCollection<MeasurementProcedure<? extends Measure, ? super CorrelationDescriptor>> additionalMeasurementProcedures() {
				return measurementProcedures;
			}
		};

		abstract Predicate<SearchNode> additionalPruningCriterion();

		abstract ImmutableCollection<MeasurementProcedure<? extends Measure, ? super CorrelationDescriptor>> additionalMeasurementProcedures();

	}

	public static enum OptimisticEstimatorOption {

		OLD {
			private double optEst(SearchNode node) {
				double expectedMI = node.pattern.value(EXPECTED_MUTUAL_INFORMATION);
				double entropyOfY = node.entropyY;
				return 1 - expectedMI / entropyOfY;
			}

			@Override
			ToDoubleFunction<SearchNode> fOEst() {
				return this::optEst;
			}

		},

		NEW {
			private double optEst(SearchNode node) {
				CorrelationDescriptor descriptor = node.pattern.descriptor();
				ContingencyTable table = descriptor.contingencyTable();

				return 1 - ContingencyTables.expectedMutualInformationUnderPermutationModel(node.marginalY, table)
						/ node.entropyY;
			}

			@Override
			ToDoubleFunction<SearchNode> fOEst() {
				return this::optEst;
			}
		};

		abstract ToDoubleFunction<SearchNode> fOEst();

	}

	private ToDoubleFunction<SearchNode> f = p -> {
		return p.pattern.value(p.pattern.functionalityMeasure());
	};

	// private ToDoubleFunction<SearchNode> fOEst = p -> {
	// double expectedMI = p.pattern.value(EXPECTED_MUTUAL_INFORMATION);
	// double entropyOfY = p.pattern.value(CODOMAIN_ENTROPY);
	// return 1 - expectedMI / entropyOfY;
	// };
	//
	// private ToDoubleFunction<SearchNode> fOEst2 = p -> {
	// CorrelationDescriptor descriptor = p.pattern.descriptor();
	// ContingencyTable table = descriptor.contingencyTable();
	// ContingencyTable marginalY = table.marginal(descriptor.domain().size());
	//
	// return 1 -
	// ContingencyTables.expectedMutualInformationUnderPermutationModel(marginalY,
	// table)
	// / marginalY.entropy();
	// };

	public Optional<Double> value(ComputationMeasure measure) {
		if (opus != null) {
			return opus.value(measure);
		} else {
			return Optional.empty();
		}
	}

}
