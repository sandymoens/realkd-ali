/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-16 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.algorithms.branchbound;

import de.unibonn.realkd.algorithms.ComputationMeasure;

/**
 * @author Mario Boley
 * 
 * @since 0.4.0
 * 
 * @version 0.4.0
 *
 */
public enum BranchAndBoundMeasures implements ComputationMeasure {

	NODES_CREATED("Created nodes", "The number of nodes that have been created and evaluated by the algorithm."),
	NODES_DISCARDED_POTENTIAL("Discarded nodes from potential", "The number of nodes that have been discarded after evaluation due to insufficient potential."),
	NODES_DISCARDED_PRUNING_RULES("Discarded nodes from pruning rules", "The number of nodes that have been discarded before evaluation from pruning rules."),
	MAX_DEPTH("Max depth", "The maximal depth of an evaluated search node."),
	SOLUTION_DEPTH("Solution depth", "The depth of the best node discovered. In case of top-k results, it indicates the first encounter of a best solution."),
	MAX_SIZE_BOUNDARY_QUEUE("Maximum size of boundary queue", "The maximum size the boundary queue reached while searching for solution.");

	
	private final String caption;

	private final String description;

	private BranchAndBoundMeasures(String caption, String description) {
		this.caption = caption;
		this.description = description;
	}

	@Override
	public String caption() {
		return caption;
	}

	@Override
	public String description() {
		return description;
	}

	
}
